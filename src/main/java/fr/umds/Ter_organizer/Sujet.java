package fr.umds.Ter_organizer;

/**
 * 
 */
public class Sujet {

    /**
     * Default constructor
     */
    public Sujet() {
    }

    /**
     * 
     */
    private String nom_;

    /**
     * 
     */
    private Enseignant enseignant_;

    /**
     * 
     */
    private Groupe groupe_;

    /**
     * 
     */
    private Enseignant rapporteur_;



    /**
     * parameterized constructor
     * @param nom
     * @param enseignant
     */
    public Sujet(String nom, Enseignant enseignant) {
    	nom_=nom;
    	enseignant_=enseignant;
    	
    }



    /**
     * @param groupe
     */
    public void setGr(Groupe groupe) {
    	groupe_=groupe;
    }

    /**
     * @param rapporteur
     */
    public void setRap(Enseignant rapporteur) {
        rapporteur_=rapporteur;
    }

    /**
     * 
     * @return
     */
    public String getN() {
        return nom_;
    }

    /**
     * 
     * @return
     */
    public Enseignant getE() {
        return enseignant_;
    }

    /**
     * 
     * @return
     */
    public Groupe getG() {
        return groupe_;
    }
    
    /**
     * 
     * @return
     */
    
    public Enseignant getR() {
    	return rapporteur_;
    }

}